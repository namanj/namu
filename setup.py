from distutils.core import setup

setup(
  name = 'pyNamu',
  packages = ['pyNamu'], # this must be the same as the name above
  version = '0.1',
  description = 'My First Package!',
  author = 'Naman Jain',
  author_email = 'namanjn07@hotmail.com',
  url = 'https://github.com/peterldowns/mypackage', # use the URL to the github repo
  download_url = 'https://github.com/peterldowns/mypackage/tarball/0.1', # I'll explain this in a second
  keywords = ['testing', 'logging', 'example'], # arbitrary keywords
  classifiers = [],
)
